+++
date = "2020-06-14"
title = "Trying Uncomfortable Things"
slug = "trying-uncomfortable-things"
tags = ["personal", "culture"]
categories = []
series = []
+++

I'm a huge believer that seeking uncomfortable situations, aka "going out of your comfort zone" is crucial to personal growth. OK, everyone knows this. But think for a moment - when was the last time you actively *sought* to do something uncomfortable, just to challenge yourself?

While [these guys](https://www.youtube.com/watch?v=WtJxVrr2F8M) take it to the extreme, you don't have to go that far to see positive results. [One person](https://www.youtube.com/watch?v=-vZXgApsPCQ) sought rejection for 30 days by asking people to do weird things for the sole purpose of getting rejected. Spoiler: a few months of doing this changed his life.

Here are some things I've realized from my experiences:

- I learned a ton about myself. How I reacted to situations, realizing I don't know myself as well as I thought, and how I adapted to discomfort.
- Even small experiments help. Examples: ordering something different than your go-to at a restaurant, taking a cold shower one morning, or randomly approaching a stranger to make conversation.
- It makes life more interesting.

## Some personal stories:

1: First day of kindergarten, age 5. Having to learn two new languages at once and getting weird stares for being one of the only Asians in class. At recess, I remember talking to other kids in Korean while they spoke to me in Portuguese. We didn't understand a word we said to each other, but somehow we got along.

2: Moving to the US for college. Even though I was fluent in English by then, I didn't realize that language was a very small part of adapting to the culture. I'll never forget the first time hearing a southern accent at my college cafeteria. The concept that I was fluent in a language yet could not understand them based on their accent was super confusing.

3: Arriving in France for an internship and realizing no one actually spoke English (in my defense, the interview was in English!). I vividly remember the panic I felt that entire first week. Eventually I picked myself up and did my homework. I spent every night that summer studying French through old books and Youtube videos. Shout out to all the people in Étampes (a town in northern France) who were super nice and welcoming to me, literally the only Asian kid in town that summer.

4: Moving to Chile for 6 months on a week's notice. Within months of starting my first job post college, my manager goes, "Have you ever been to Santiago? No? Great!". And off I went to a new place. I barely spoke Spanish. There I was, a 23 year old, trying to present myself professionally to C-level executives, in a language and culture I was not familiar with. On top of that, the project manager didn't speak English. We'd spend all day sitting side by side, unable to understand each other. We resorted to communicating via Skype with the Google Translate tab open. Eventually, by necessity, I picked up Spanish as well.

5: Traveling alone. I learned that I could be outgoing if I wanted to, even though I considered myself a quiet introvert. I learned to be comfortable being alone and to deal with various moments of loneliness during that trip.

6: That time my roommate and I shared a junior bedroom (basically a studio with wall separation between the kitchen and bedroom) for over 6 months. We moved to a smaller place to save money and motivate ourselves to quickly find a job in Silicon Valley. It took me 1.5 years before I found one. That was one of the most mentally tough things I ever did.

7: Quitting a six figure salary job at a large tech company at 29 years old to reset my career as a software developer. I enrolled in a coding bootcamp, where I was one of the oldest students.

8: After the bootcamp, I applied to over 150 companies and promptly got rejected by 90% of them. At the end of my first ever coding interview, their feedback was, verbatim, "You're really bad at this". I learned to stay humble and deal with imposter syndrome.

9: Writing a blog and putting my thoughts out there publicly 👶🏻.

I hope this inspires at least one person out there to seek uncomfortable situations more often. It's intimidating, but if an introvert like myself can do it, so can you!