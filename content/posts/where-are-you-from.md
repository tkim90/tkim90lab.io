+++
date = "2020-05-25"
title = "Where are you from?"
slug = "where-are-you-from"
tags = ["personal", "culture"]
categories = []
series = []
+++

When I was 24, I traveled to Europe alone.

Planning a trip with friends is tough. We'd been pushing off so much that I decided to go by myself. It was intimidating, but at the time I was all about challenging my comfort zone™. I chose to go somewhere I knew nothing about but had been interested in for a while - Germany and Scandinavia. I didn't speak the languages, I didn't know anyone in the area, and had zero reference as to what it would be like.

Feeling adventurous, I booked my flight without any accommodation plans in advance (read: I procrastinated). Rooms were nearly filled for the summer, so my only option was to stay at cheap hostels and move every few days. By the end of the trip, I had stayed at over 10 hostels.

### Social by design

One of the best features of hostels is that they're designed to help guests meet new people. Rooms are shared, they have large hangout areas, and they organize events that lead to natural conversation between strangers. At around 6PM every day, people from all around the world congregate in the kitchen, patiently waiting their turn to cook their food, relax, and share stories. This was the highlight of my day because I finally got to socialize after spending hours exploring by myself.

One hostel in Stockholm had a particularly large kitchen with wide picnic tables. By 6PM, people gathered around the tables and introduced themselves. Instead of joining in, I kept quiet and awkwardly waited my turn to cook my feast for the night - pasta and tomato sauce.

Eventually, another traveler noticed me sitting alone and joined me. With a wide smile, he reached his hand out and introduced himself in a thick French accent.

"Ello, my name is Frederic! What is your name? Where are you from?"

### "Where are you from?"

Although this question is meant as a simple conversation starter, I found it surprisingly difficult to answer.

You see, I have a "complicated" background. Complicated in the sense that it can't be answered with a single sentence and often requires more follow up details for a satisfying answer. I was born in Korea, moved to Brazil as a baby, and moved to the US for college where I lived at the time. Although not that complicated to explain, it was often tricky as a quick introduction.

Over the course of the trip, I found myself getting asked that same question over and over. I started to think about what an ideal answer would look like. How could I narrow it down to one "go-to" answer that was concise and satisfying enough?

I decided to make an experiment out of it. I tried different answers to the question and observed how people reacted. The more satisfied they were by the answer, the more likely I'd use it again.

### The Experiment

Let's quickly break down the question:

When they ask "where are you *from*", are they asking about my heritage, where I grew up, or where I live now? After all, I'm Korean by blood, but identify myself as Brazilian (I grew up there), and now live in the US. It gets tricky because different people expect different answers, but you don't know which one they're looking for!

1 - "I'm Korean"

**Reaction:** they'll notice my American accent and say something like "But where did you grow up?". Which leads me to my follow up answer (which always catches people off guard): "I grew up in Brazil". See #3 for further reactions.

2 - "I live in the US"

**Reaction:** "But where are you *really* from", prodding at the fact that I'm Asian, therefore which Asian am I? Why they feel the need to ask that to a person they just met, I don't know. My best guess is that they're trying to find something in common by sharing something they know about my country, but unfortunately it comes off as combative. Please don't do this.

3 - "I'm Brazilian"

**Reaction:** "😯😐???". People got very confused. You look Asian, but you're from South America? Something's not right here [0]. From there, the conversation would go one of two ways - either they ignore the response and move on or prod me with more questions about my experience there.

After meeting tens of people over the course of 2 weeks, here are my random observations:

- Saying I was Brazilian was definitely the most controversial. One person even got offended thinking I was lying to them.
- People try to fit you into a mental box of stereotypes. They use clues as confirmation that you fix in one box or another. American accent? You're probably American.
- Most people were satisfied with "I'm Korean", so I stuck with that. I realized many people didn't actually care about the answer; they're just making conversation.

Some memorable reactions:

- One person tried their best not to act surprised, probably out of politeness. But they were clearly confused.
- One person thought I lied when I said I was Brazilian. An hour into the conversation, while I described life in Brazil, and they go, "wait, you're really from Brazil? I thought you were lying!".
- After saying I'm Brazilian: "Oh yeah? Say something in Portuguese". Sorry, I don't have to prove myself to someone I just met.
- All the Brazilians I met along the way - they're always happy to see another fellow Brazilian abroad!

### Conclusion

I used to think about this question a lot. How can I answer this in a way that satisfies the other person? Over time, as I grew more confident about my identity, I learned not to think too much of it. After all, why should I care if they believe who I say I am?

Besides that, I had a lot of fun meeting so many interesting people from around the world. The best memories were not the fancy museums or landmarks I visited; they were the connections I made with people from countries I thought I'd have nothing in common. The hours of conversation and life perspectives we shared based on our unique backgrounds. The realization that despite our different culture, language, and mentality, we always found something in common.

Where are you from? I used to be jealous of people who could answer this question without hesitation. I used to be jealous of people who weren't questioned when they gave an answer. I used to be jealous that people had a clear "team" to cheer to at soccer matches - their appearance, accents, mannerisms matched stereotypes of the countries they came from. But over time, I embraced who I was, both Korean and Brazilian. Now, whenever someone asks me "where are you from?", I can't help but smile.

---

[0] Fun fact: the largest Japanese population outside of Japan [is in Brazil](https://en.wikipedia.org/wiki/Japanese_Brazilians). Just like there's tons of Asians in the US, you guessed it: there's tons in South America too!