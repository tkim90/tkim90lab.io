+++
date = "2020-02-19"
title = "Practical Philosophy: Stoicism"
slug = "practical-philosophy-stoicism" 
tags = ["philosophy"]
categories = []
series = []
+++

I've been reading a lot about Stoicism and have been pleasantly surprised at how relatable its teachings are. I'm not a philosophy buff and have actually found them depressing at times (see Nietzsche, Sartre, Camus, etc.), but I found Stoicism surprisingly effective and easy to apply to my daily life.

## What is Stoicism?

Stoicism teaches the idea that emotion should be controlled through reason, which ultimately allows people to lead better lives. Unlike most philosophies, Stoicism is about *doing* more than thinking. In short, it's a collection of practical wisdom on how to lead a happy and tranquil life.

A common misconception is that stoics suppress their emotions and accept their situation as it is. This is far from true - in reality, stoicism teaches you to be constantly aware of your impulsive reactions and self defeating thoughts and challenges you to evaluate them through a rational, objective lens.

!["Stoic Calvin & Hobbes](/images/stoic-calvin-and-hobbes.jpeg)

## Stoic Teachings That Have Helped Me

1. **You can't control things that happen to you, but you can control how you react to them.** This to me is the main idea in stoicism. Once you realize that most things that make you unhappy or anxious are your *choices* more than facts of life, you will be much better off.

    > *"Everything can be taken from a man but one thing: the last of the human freedoms - to choose one's attitude in any given set of circumstances." - Victor Frankl*

2. **Events can't hurt us - it's how we perceive them that do.** Change your perspective to frame everything as a growth opportunity. For example, if someone is being difficult, treat it as an opportunity to practice patience and understanding. Ask yourself, "what can I learn from this unpleasant experience?".
3. **Fear comes from uncertainty; we suffer more from our own imagination than reality.** When you're worried about something, write down the answers to the question, "what are the absolute worst things that could happen to me?". 

    Most times, they're manageable and not as bad as you thought. If the thought still bothers you, write down 3 possible alternatives or solutions to those problems. The mere act of writing them down will make you feel better.

4. **Resist the urge to be outraged by everything.** It's not worth it. This one is tough for me, especially since I'm addicted to staying informed about everything all the time. This doesn't mean you should abandon news altogether - but make sure that you deliberately choose the topics you care about most, and ignore the rest.

    > *"You always own the option of having no opinion. There is never any need to get worked up or to trouble your soul about things you can't control. These things are not asking to be judged by you. Leave them alone." - Marcus Aurelius*

5. **It's all temporary.** Learn to appreciate the good, brave through the bad, but know that all things in life will end. If you find yourself very upset about something that didn't go your way, remind yourself that a year from now, you'll probably shrug or laugh about it.

    > *"Alexander the Great and his mule driver both died and the same thing happened to both." - Marcus Aurelius*

## Examples in my life

Here are some situations where stoicism really helped me!

- **Dealing with negative people.**

    I once worked with a very negative colleague. They would go out of their way to criticize my work in front of others during meetings. I remember being stressed out in the mornings before work because I'd be thinking about what this person would say next.

    One day, I was leading a meeting when they interrupted me and started complaining about a task they thought I had done incorrectly. Rather than letting them get to me for the nth time, I paused, took a few seconds to process my reaction, and simply replied, "OK. Anyways..." and continued the meeting. And just like that, I neutralized their negativity and moved on with my life. I did this a few more times until they eventually stopped altogether.

    I learned that you have the choice to waste your energy consuming others' negativity or to prevent it from affecting you.

- **Tackling anxiety that came from uncertainty.**

    By far one of the most anxiety inducing topics in my life is immigration. I've lived in the US for 10+ years and still have to wonder if I'll be able to stay in the country next year or if I'll have to pack up my life and leave in a moment's notice. I've had so many sleepless nights and rants to friends about how stressful the US immigration system was. I felt helpless.

    At one point, I decided to take action and write down worst case scenarios. If I really had to leave the country tomorrow, what would that look like? I even contacted a private lawyer to discuss my options. Guess what? Not getting my visa renewed was not the end of the world - if I had to leave, not only were there so many other great countries in the world to move to (I am single with no kids), I could also come back through different visas later on (there were several options to choose from).

    Immediately I saw anxiety reduce because I was more informed. Having back up plans and knowing that the worst is not so bad really helped me.

## Closing Thoughts

It's in our nature to behave emotionally. But do we have to suffer as much as we do for it? Probably not. Stoicism is an incredibly simple tool that helped me navigate difficult situations and lead a more anxiety-free life - hopefully it helps you as well!