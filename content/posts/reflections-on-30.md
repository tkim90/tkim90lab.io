+++
date = "2020-01-30"
title = "Reflections on 30"
slug = "reflections-on-30" 
tags = ["personal"]
categories = []
series = []
+++

I turned 30 this year. As I reflect on my 20s, I prepared a list of 30 random things that I've learned that helped me become a better person.

1. Self awareness is key to personal growth.

2. Live deliberately and proactively. If there's something about your life you don't like, write down ways to change it and make them happen. Own your outcomes!
3. Politics: Instead of trying to change people's minds, try to *understand their position*. What kinds of life experiences did they have that led to those conclusions? What about yours - do you see how you could be biased? The goal should be to understand each other and see where each side could have been  misunderstood.
4. Empathy and understanding is very effective for resolving conflicts. When people are upset, chances are they're not saying what they're actually thinking. Most times, all you have to do is listen.
5. Practicing curiosity makes life more interesting.
6. Journaling is amazing for mental health.
7. Your mental health is much more important and fragile than you think - no matter how busy you are, make time for activities like hobbies, time with friends/family, and self-reflection. You won't regret time spent for yourself.
8. Always assume positive intent (found this especially applicable at work).
9. Focus on building long term relationships. Studies show relationships are the source of a happy life!
10. Vulnerability strengthens relationships.
11. Clear writing = clear thinking.
12. A small change in perspective goes a long way.
13. Going on walks is great for slowing down, thinking, and clearing your mind.
14. It doesn't matter how smart you are if you're not kind to people.
15. Learning how to learn is an accessible skill that everyone should know about (see "A Mind for Numbers" by Dr. Barbara Oakley). Actively recall info you learned, use spaced repetition with flash cards, take breaks, and much more. I'm baffled why schools don't teach this.
16. You enjoy different books based on your mood and moment in life - novels are fun to lose yourself in a different world, and information heavy books are fun when you take your time to think about it as you read it. You shouldn't feel guilty for preferring one or the other.
17. "To get more out of life, don't work *harder*, work *efficiently" - Ray Dalio
18. People are a lot more open to helping you than you think. You just have to ask for help (make sure you do your research beforehand, though).
19. Life is much more exciting when you go out of your comfort zone.
20. Most times, the best way to deal with negativity is to ignore it.
21. The modern use of the word "passion" is overused and misunderstood. You need to experiment A LOT before you find the work you enjoy doing.
22. Your career doesn't have to follow a linear trajectory. Your interests evolve as you grow, and that's OK.
23. Know your values (How should I prioritize my time? What makes me happy?). Realize that they change over time.
24. Practicing compassion helps you be more patient and understanding. Everyone has their own battles you don't know about.
25. The type of food I eat strongly influences my mood and ability to focus.
26. The world is a very complicated and messy place. The answer to any question will always be "it depends".
27. You decide how you react to situations. Life isn't fair, but unfortunately it doesn't give a damn either.
28. Technological innovation is exciting, but often has a lot of scary implications that not enough people are thinking about (see: Black Mirror).
29. Humans are basically biased, validation-seeking, tribal animals. We can try our best to minimize them, but it's impossible to remove them completely.
30. Without your health, you can't do anything else. Take care of your body!