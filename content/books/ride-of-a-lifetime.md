+++
date = "2020-05-29"
title = "The Ride of a Lifetime - Bob Iger"
slug = "ride-of-a-lifetime" 
tags = ["books"]
categories = []
series = []
rating = "⭑⭑⭑⭑⭑"
+++

Rating: ⭑⭑⭑⭑⭑

In "A Ride of a Lifetime", Bob talks about his career in the media industry leading up to his 14 years as CEO of Disney. Marketed as a business book, it gives readers an insider view to his experience as a leader in a multi billion dollar company that we all grew up watching. Although at first glance this might seem like a generic business book, it's much more than that.

Throughout the book, Bob comes off as a genuine, humble, and relentlessly hard working person that it's not surprising that he's successful. I really enjoyed how candid he is in his narrative, talking openly and honestly about his thought process and insecurities throughout his career.

Here is just a short list of accomplishments that I'm sure all of you are familiar with:

- Disney acquiring Pixar, resulting in award winning movies like Toy Story and Frozen
- Disney acquiring Marvel, resulting in The Avengers movies
- Disney acquiring LucasFilm and the legendary Star Wars series
- Disney acquiring 21st Century Fox
- Partnered with Steve Jobs to put ABC shows on his new product (a crazy idea at the time), the iPod, in 5 months (from conceiving the idea to launching to market)
- Disney launching their Netflix competitor, Disney Plus (and digitizing all other channels)

Do you see a pattern here? Bob was a world class performer in three things: finding potential in multi-million dollar ventures, convincing powerful people to join him, and executing it flawlessly.

But there is one thing that stood out to me that made this book unique: his ability to empathize. Countless times, he shares his war stories with so much self-awareness and compassion. Whether it's navigating a difficult negotiation, firing someone, or making a case for a huge acquisition, Bob always made sure that the other person truly felt listened to.

I found this insight useful because I believe that's the secret sauce to being successful in any career. When I was a PM at Tesla, I had to work with very demanding people, navigate conflict, and coordinate between tens of teams that didn't always like working with each other. It was especially hard for me because I'm an introvert. But I learned so much from it. When resolving conflict, I found that empathy was not only the most effective method, but also the one with the most lasting positive effects. When things get tough, rather than thinking of tactics to undercut and "defeat" them, appeal to their humanity by listening to them. Really listen to what they have to say and also take away what they're not saying. Most times, all you need to do is listen to them, because no one else would. That alone will get you their respect.

In one of Bob's conflicts, he had to convince Roy (an old colleague) not to sue his company for millions of dollars for disrespecting him. Roy was apparently very stubborn and didn't want to talk to anyone - they couldn't get him on the phone without involving his lawyer. By the end of that episode, Bob somehow convinced Roy to stand down. This is what he had to say about that experience:

> "If you approach and engage people with respect and empathy the seemingly impossible can become real."