+++
date = "2020-04-28"
title = "Stoner - John Williams"
slug = "stoner-john-williams" 
tags = ["books"]
categories = []
series = []
+++

Rating: ⭑⭑⭑⭑⭑

I'll admit that this book caught me by surprise. 

I didn't think I'd enjoy this book when I picked it up - the synopsis just doesn't seem interesting. That's what I get for judging a book by its cover (literally)!

Stoner is about a man living an ordinary life and dealing with its endless disappointments. Despite the simple premise, the author's beautiful writing, insights, and relatable thoughts made this a very enjoyable read. If you're an introvert, I think you'll appreciate this especially more. Williams has a way with words that is easy to read yet beautifully written - much of it feels like you're either catching up with an old friend or a grandparent recounting their life story. For reasons I can't quite explain, you can't help but end up invested in what the author has to say.

I'm trying to slowly change my habit of judging classics prematurely - every time I see one, I instinctively think of boring, old, sad people writing about life centuries ago that I couldn't imagine myself relating to. But what makes them classics in many ways is precisely *because* they're so relatable, even after so many years. Note to self - read more classics.

Towards the end of the book, I was surprisingly sad that the story ended. As I closed the book, I felt nostalgic for a life lived by someone else, for a character that never existed. It was an odd feeling, and rare for books to affect you that way.

## What it's about

Born in the early 1900s to a poor farmer family in Missouri, William Stoner was a quiet teen with no particular aspirations in life. On the advice of his parents, he goes to college to study agriculture to eventually return and help grow the farm.

To emphasize how "ordinary" his life is, these are the first lines in the book:

> *William Stoner entered the University of Missouri as a freshman in the year 1910, at the age of nineteen. [...] He did not rise above the rank of assistant professor, and few students remembered him with any sharpness after they had taken his course… Stoner’s colleagues, who held him in no particular esteem when he was alive, speak of him rarely now; to the older ones, his name is a reminder of the end that awaits them all, and to the younger ones it is merely a sound which evokes no sense of the past and no identity with which they can associate themselves or their careers.*

Unrelated note: this intro, along with a lot of Stoner's thoughts, is similar to the ideas in "Death of Ivan Ilyich". Maybe a nod to Tolstoy?

## Disappointment & Expectation

This book is about disappointment. Throughout his life, Stoner constantly disappoints people around him, even if it's not always his fault. At an early age, he decides not to study agriculture like his parents expected him to. He hastily marries a woman (Edith) who mistreats him and results in an unhappy marriage. He falls in love through an affair, but is forced to end it due to the risk of losing his family and career. Even his daughter, whom he loves dearly, turns against him and becomes a resentful alcoholic.

What I took from this is to temper your expectations and not beat yourself up when life doesn't go according to plan. Though it is portrayed extremely in Stoner's case, asking himself "What did you expect?" is a thought I also had many times before.

## Searching for Happiness

Finally, this book is about searching for happiness. People expect to get to that ultimate moment in life where you get everything you wanted and live happily ever after. In reality, most of our life is mundane, sprinkled with a few highlights of extreme happiness or sadness in between. "Stoner" portrays this reality pretty accurately - he's ecstatic when marrying his wife, but slowly ends up in an unhappy marriage; he has a lovely daughter, who ends up resenting him as she grows older; he has an affair with a woman he loves, which inevitably comes to an end.

To me, this was a reminder for two things. One, don't miss out on the present by constantly daydreaming about the future - savor the present *[1]*. And two, life is actually "boring" for the most part - and that's OK! In fact, it's unrealistic to expect life to be exciting all the time. Social media is largely to blame here.

## Takeaways

What struck me most about "Stoner" is that it *feels* sad, but it's not. It shows that even an ordinary life can have a beautiful story.

This is especially important in today's social media age, where it seems like people who lead "cool lives" have stories worth telling, while us mortals who don't have thousands of followers, don't.

After spending the rest of the night writing this review, I still can't quite find the words to describe why I liked this book so much - you'll just have to read it and decide for yourself!

#### Footnotes
[1] The course *The Science of Wellbeing* teaches that humans are mostly only happy from temporary events because we're "doomed" to get used to things that linger (ex. marriage - being happy in the first 2 years then plateauing, buying a shiny new car and eventually getting used to owning it, etc.).

### Quotes

“Sometimes, immersed in his books, there would come to him the awareness of all that he did not know, of all that he had not read; and the serenity for which he labored was shattered as he realized the little time he had in life to read so much, to learn what he had to know.”

“In his forty-third year William Stoner learned what others, much younger, had learned before him: that the person one loves at first is not the person one loves at last, and that love is not an end but a process through which one person attempts to know another.”