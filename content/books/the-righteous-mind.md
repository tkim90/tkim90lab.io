+++
date = "2020-02-09"
title = "The Righteous Mind - Jonathan Haidt"
slug = "the-righteous-mind" 
tags = ["books"]
categories = []
series = []
+++

My Rating: ⭑⭑⭑⭑⭑

In 10th grade, I took a Morality class. In it, we discussed topics like white lies, bullying, and cheating. We talked through various scenarios and tried to fit them somewhere in the right/wrong spectrum. 

In the end, rather than leaving that class with a clear sense of how to reason through right and wrong, I was frustrated because it felt like I was given a list of rules to follow "just because".

Since that class, I thought a lot about morality and its role in society. Why is it useful? Who decides whether something is right? Can something be "universally" right or wrong? Does it apply across generations? For example, if killing is wrong, why does it automatically become "normal" during war? Haidt answers these questions and discusses morality through the lens of psychology.

I thoroughly enjoyed it even though I didn't agree with everything in the book. This book made me realize mistakes in how I think, how biased I am but tell myself otherwise, and how I should spend more time researching arguments from the other side. In the end, I left this book with a feeling of compassion for people in general and a better understanding of why everything seems so polarizing in 2020. Most importantly, I've since approached disagreements as opportunities to learn why the other side thinks that way, and where the disconnect is between us.

## Main Idea

(1) Humans are irrational and biased. We make emotional snap judgments *first* and gather reasons to support those snap judgments *second*.

- Humans are tribal, validation-seeking animals. Because of this, people's self interest and concern for reputation greatly influences moral reasoning. This makes us hypocrites!

    > "Moral talk serves a variety of strategic purposes such as managing your reputation, building alliances, and recruiting bystanders to support your side in the disputes that are so common in daily life."

    > "Our moral thinking is much more like a politician searching for votes than a scientist searching for truth. If you think that moral reasoning is something we do to figure out the truth, you'll be constantly frustrated by how foolish, biased, and illogical people become when they disagree with you."

- Groupthink is incredibly dangerous. Our shared ideals make us very cohesive as a group, but blind us from the ideals of others. Why do some people get offended when a football player kneels during the American anthem, and others see it as a justified way of protesting?

    > Morality binds and blinds.

(2) There are 5 moral "taste receptors" (per Haidt) that make up a person's morality. Which ones you weigh more indicates whether you lean politically left, center, or right.

- Care
- Fairness
- Loyalty
- Authority
- Sanctity/Purity
- The Left value the following the most: Care/Harm, Liberty/Oppression, Fairness/Cheating
- The Right value the following the most: Loyalty/Betrayal, Authority/Subversion, Sanctity/Degredation

*Insert Haidt chart on each based on where you lean

Birth control: care/harm vs authority

Gun violence: care/harm vs liberty/oppression

Kneeling during the anthem: care/harm vs loyalty, authority, sanctity

## Takeaways

The author's definition of morality made this topic a lot easier to think about. 

**Morality is a set of man-made rules on what they think will result in a harmonious society.**

So back to the question, "Can something be 'universally' right or wrong?" - no, because morality varies based on the society you're in. A tribe might say that it's right to sacrifice humans to serve a god they believe in - for others, taking any human life is immoral. Beating a wife because they were disobedient could be seen as acceptable in some countries, but that would cause outrage in others. Morality is a set of man-made rules - so we as a society decide what's right and wrong based on our values.

This leads to a very interesting observation I made on how much our environment (geography, culture, socioeconomic status) shapes our morality. Specifically, Western countries tend to favor a culture of individualism while Eastern countries favor collectivism.

Test yourself with the following: say a girl goes to a school that requires uniforms. If she decides to wear something other than the uniform, is that OK? What if her teacher said it was OK?

Societies that favor individualism (the west) will say she can dress whatever she wants, because she has that freedom to choose. Societies that favor collectivism (the east) will say that it's not OK, because you're disrespecting the community you're in.

People don't disagree with you because they despise you or because they're evil. I remember countless situations where I thought, "that person is just out to get me". Instead of assuming people are bad, they probably have good reasons for why they disagree with you. Thinking about disagreement this way helped me be more patient and understanding with others.

## Random interesting facts/insights

- The term political "right" and "left" comes from the French Assembly of 1789, where the delegates who favored preservation sat at the right side of the chamber, while those who favored change sat on the left.
- Europeans banned dancing in Christianity in the Middle Ages, theorized due to the increased notion of the sense of self (away from group-ism). They were also disgusted by religious dancing they saw with the natives.
- Cities tend to be more liberal than the countryside because it attracts more people who are open to new experience and are less sensitive to threats.
- Startups (mostly tech startups) have "culture codes", because they want to recruit people that think like them (ironic, given the progressive move towards diversity). This will make them more cohesive and more likely to succeed as a group.
- From a liberal standpoint, too much individualism (ex. diversity) tends to break apart the cohesiveness of the group and therefore the group's effectiveness in competition with other groups.
- From a conservative standpoint, too much collectivism in group morality tends to harm certain individuals more at the expense of benefitting the group as a whole.
- On the *purpose* of morality across different schools of philosophy:
    - **Rationalists** think morality (guided by reason) helps us find *truth* in the right way to behave.
    - **Glauconians** think morality serves to help humans pursue our *socially strategic goals* (guided by our desire to be respected, have a positive reputation).
- Definition of ideology: "A set of beliefs about the proper order of society and how it can be achieved."
- The Orthodox ideology believes that there is a "transcendent moral order, to which we ought to try to conform the ways of society" - ex. Christians who look to the Bible as a guide for legislation, like Muslims who want to live under sharia, are examples of orthodoxy. Basically, orthodox religious groups think there is in fact a "universal moral code", and anyone who doesn't follow them is wrong.
- People can be "morally dumbfounded". Haidt gave his subjects a series of situations and ask them if it was moral or not. In some cases, subjects would respond with **"I don't know why it's wrong, but I just know it is!"** (i.e. intuition clouding their ability to rationalize objectively). Example:
    - If a woman cut up the US flag and used it as a rag to clean her bathroom, and there was no one there to see it, is that wrong?

## Quotes to think about

- "Morality binds and blinds."
- "Our moral thinking is much more like a politician searching for votes than a scientist searching for truth. If you think that moral reasoning is something we do to figure out the truth, you'll be constantly frustrated by how foolish, biased, and illogical people become when they disagree with you."
- "We do moral reasoning not to reconstruct the actual reasons why we ourselves came to a judgment; we reason to find the best possible reasons why somebody else ought to join us in our judgment"
- "Moral talk serves a variety of strategic purposes such as managing your reputation, building alliances, and recruiting bystanders to support your side in the disputes that are so common in daily life"
- "...appearing concerned about other people's opinions makes us look weak, we (like politicians) often deny that we care about public opinion polls. But the fact is that we care a lot about what others think of us. The only people known to have no sociometer are psychopaths."
- "Everyone cares about fairness, but there are two major kinds. On the left, fairness often implies equality, but on the right it means proportionality - people should be rewarded in proportion to what they contribute, even if that guarantees unequal outcomes."
- "... a taxi driver who told me that he had just become a father. I asked him if he planned on staying in the United States or returning to his native Jordan. I'll never forget his response: "We will return to Jordan because I never want to hear my son say 'fuck you' to me."
- "just because something is disgusting, doesn't make it wrong"
- "Plagues, epidemics, and new diseases are usually brought in by foreigners - as are many new ideas, goods, and technologies - so societies face an analogue of the omnivore's dilemma, balancing xenophobia and xenophilia."
- "Moral systems are interlocking sets of values, virtues, norms, practices, identities, institutions, technologies, and evolved psychological mechanisms that work together to suppress or regulate self-interest and make cooperative societies possible."
- "Beware of anyone who insists that there is one true morality for all people, times, and places - particularly if that morality is founded upon a single moral foundation. Human societies are complex; their needs and challenges are variable. Our minds contain a toolbox of psychological systems, including the six moral foundations, which can be used to meet those challenges and construct effective moral communities."